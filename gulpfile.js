'use strict';
var gulp = require('gulp');
var babel = require('gulp-babel');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var fs = require('fs');
var esperanto = require('esperanto');
var karma = require('gulp-karma');

gulp.task('bundle', function() {
  
  return esperanto.bundle({
    base: 'src',
    entry: 'main',
  }).then(function(bundle) {

    bundle = bundle.concat({
      sourceMap: true,
      sourceMapFile: 'bundle.js.map'      
    });

    fs.writeFileSync('tmp/bundle.js', bundle.code);
    fs.writeFileSync('tmp/bundle.js.map', bundle.map.toString());
  
  });
});

gulp.task('build', function() {
  return gulp.src('tmp/bundle.js')
    .pipe(sourcemaps.init({loadMaps: true, debug: true}))
    .pipe(babel())
    .pipe(rename('synge.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('dist'));
});

gulp.task('test', function() {
  return gulp.src('./testsYo')
    .pipe(karma({
      configFile: 'karma.conf.js',
      action: 'run'
    }))
    .on('error', function(err) {
      console.log(err);
      this.emit('end');
    });
});

gulp.task('default', function() {
  
  gulp.watch('tmp/bundle.js', ['build']);
  gulp.watch('src/**/*.js', ['bundle']);
  gulp.watch('test/**/*.spec.js', ['test']);
  
});