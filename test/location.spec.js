describe('Location Module', function() {
  
  it('Should exist', function() {
    expect(typeof Synge.Location).toBe('function');
  });

  it('Should have 3 dimensions', function() {
    var loc = Synge.Location({x: 5, y: 4, z: 3});
    expect(loc.x).toEqual(5);
    expect(loc.y).toEqual(4);
    expect(loc.z).toEqual(3);
  });

  it('Should allow any dimension to be undefined', function() {
    var loc = Synge.Location();
    expect(loc.x).not.toBeDefined();
    expect(loc.y).not.toBeDefined();
    expect(loc.z).not.toBeDefined();
  });
  
  it('Should store new values for each property', function() {
    var loc = Synge.Location({x: 4, y: 5, z: 6});
    
    loc.x = 1;
    loc.y = 2;
    loc.z = 3;
    
    expect(loc.x).toEqual(1);
    expect(loc.y).toEqual(2);
    expect(loc.z).toEqual(3);
  });
  
  it('Should not allow new properties to be assigned to it', function() {
    'use strict'
    var loc = Synge.Location({a: 1}),
      typeError = function() {
        loc.b = 2;
    }
    
    expect(loc.a).not.toBeDefined();
    expect(typeError).toThrowError(TypeError);
  });
})