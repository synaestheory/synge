describe('Entity Module', function() {
  
  it('Should exist', function() {
    expect(typeof Synge.Entity).toBe('function');
  });

  it('Should instatiate with explicit entity and location properties', function() {
    var entity = Synge.Entity({w: 5, h: 4, d: 3, x: 2, y: 1, z: 0});
    expect(entity.w).toEqual(5);
    expect(entity.h).toEqual(4);
    expect(entity.d).toEqual(3);
    expect(entity.x).toEqual(2);
    expect(entity.y).toEqual(1);
    expect(entity.z).toEqual(0);
  });

  it('Should allow props to be undefined', function() {
    var entity = Synge.Entity();
    expect(entity.w).not.toBeDefined();
    expect(entity.h).not.toBeDefined();
    expect(entity.d).not.toBeDefined();
    expect(entity.x).not.toBeDefined();
    expect(entity.y).not.toBeDefined();
    expect(entity.z).not.toBeDefined();
  });
  
  it('Should allow instantiation with size and location objects', function() {
    var entity = Synge.Entity({size: Synge.Size({ 
          w: 5, h: 4, d: 3
        }), loc: Synge.Location({
          x: 2, y: 1, z: 0
        })
      });
    
    expect(entity.size.w).toEqual(5);
    expect(entity.size.h).toEqual(4);
    expect(entity.size.d).toEqual(3);
    expect(entity.loc.x).toEqual(2);
    expect(entity.loc.y).toEqual(1);
    expect(entity.loc.z).toEqual(0);
  });
  
  it('Should allow instantiation with size and location objects', function() {
    var entity = Synge.Entity({size: Synge.Size({ 
          w: 5, h: 4, d: 3
        }), loc: Synge.Location({
          x: 2, y: 1, z: 0
        })
      });
    
    expect(entity.size.w).toEqual(5);
    expect(entity.size.h).toEqual(4);
    expect(entity.size.d).toEqual(3);
    expect(entity.loc.x).toEqual(2);
    expect(entity.loc.y).toEqual(1);
    expect(entity.loc.z).toEqual(0);
  });
  
  it('Should not allow new properties to be assigned to it', function() {
    'use strict'
    var entity = Synge.Size({a: 1}),
      typeError = function() {
        entity.b = 2;
    }
    
    expect(entity.a).not.toBeDefined();
    expect(typeError).toThrowError(TypeError);
  });
})