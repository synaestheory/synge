describe('Size Module', function() {
  
  it('Should exist', function() {
    expect(typeof Synge.Size).toBe('function');
  });

  it('Should have a width, height, and depth', function() {
    var size = Synge.Size({w: 5, h: 4, d: 3});
    expect(size.w).toEqual(5);
    expect(size.h).toEqual(4);
    expect(size.d).toEqual(3);
  });

  it('Should allow width, height, and depth to be undefined', function() {
    var size = Synge.Size();
    expect(size.w).not.toBeDefined();
    expect(size.h).not.toBeDefined();
    expect(size.d).not.toBeDefined();
  });
  
  it('Should store new values for each property', function() {
    var size = Synge.Size({w: 4, h: 5, d: 6});
    
    size.w = 1;
    size.h = 2;
    size.d = 3;
    
    expect(size.w).toEqual(1);
    expect(size.h).toEqual(2);
    expect(size.d).toEqual(3);
  });
  
  it('Should not allow new properties to be assigned to it', function() {
    'use strict'
    var size = Synge.Size({a: 1}),
      typeError = function() {
        size.b = 2;
    }
    
    expect(size.a).not.toBeDefined();
    expect(typeError).toThrowError(TypeError);
  });
})