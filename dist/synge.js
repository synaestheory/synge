'use strict';

(function () {
  'use strict';

  var Location = function Location() {
    var spec = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    var x = spec.x;
    var y = spec.y;
    var z = spec.z;

    return Object.seal({ x: x, y: y, z: z });
  };

  var Size = function Size() {
    var spec = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    var w = spec.w;
    var h = spec.h;
    var d = spec.d;

    return Object.seal({ w: w, h: h, d: d });
  };

  var Entity = function Entity() {
    var spec = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    var loc = spec.loc;
    var size = spec.size;
    var x = spec.x;
    var y = spec.y;
    var z = spec.z;
    var w = spec.w;
    var h = spec.h;
    var d = spec.d;
    var entity = undefined;

    if (loc) {
      loc = Location(loc);
    } else {
      loc = Location({ x: x, y: y, z: z });
    }

    if (size) {
      size = Size(size);
    } else {
      size = Size({ w: w, h: h, d: d });
    }

    entity = { loc: loc, size: size };

    Object.defineProperties(entity, {
      x: {
        get: function get() {
          return loc.x;
        },
        set: function set(x) {
          return loc.x = x;
        }
      },
      y: {
        get: function get() {
          return loc.y;
        },
        set: function set(y) {
          return loc.y = y;
        }
      },
      z: {
        get: function get() {
          return loc.z;
        },
        set: function set(z) {
          return loc.z = z;
        }
      },
      w: {
        get: function get() {
          return size.w;
        },
        set: function set(w) {
          return size.w = w;
        }
      },
      h: {
        get: function get() {
          return size.h;
        },
        set: function set(h) {
          return size.h = h;
        }
      },
      d: {
        get: function get() {
          return size.d;
        },
        set: function set(d) {
          return size.d = d;
        }
      }
    });

    return Object.freeze(entity);
  };

  //import Renderer from './renderer';
  var Synge = { Entity: Entity, Size: Size, Location: Location };

  window.Synge = Synge;
})();
//# sourceMappingURL=./bundle.js.map.map
//# sourceMappingURL=synge.js.map