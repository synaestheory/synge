export default (spec = {}) => {
  let {x, y, z} = spec;
  
  return Object.seal({x, y, z});
}