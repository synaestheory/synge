export default (spec = {}) => {
  let {w, h, d} = spec;
  
  return Object.seal({w, h, d});
}