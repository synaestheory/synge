import Location from './location';
import Size from './size';

export default (spec = {}) => {
  let {loc, size, x, y, z, w, h, d} = spec,
      entity;
  
  if (loc) {
    loc = Location(loc);
  } else {
    loc = Location({x, y, z})
  }
  
  if (size) {
    size = Size(size);
  } else {
    size = Size({w, h, d}); 
  }
  
  entity = {loc, size};
  
  Object.defineProperties(entity, {
    x: {
      get: () => loc.x,
      set: (x) => loc.x = x 
    },
    y: {
      get: () => loc.y,
      set: (y) => loc.y = y 
    },
    z: {
      get: () => loc.z,
      set: (z) => loc.z = z 
    },
    w: {
      get: () => size.w,
      set: (w) => size.w = w 
    },
    h: {
      get: () => size.h,
      set: (h) => size.h = h
    },
    d: {
      get: () => size.d,
      set: (d) => size.d = d
    }
  });
  
  return Object.freeze(entity);
}